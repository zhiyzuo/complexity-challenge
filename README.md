This repo is for my solution to the [2017 Complexity Challenge](https://www.complexityexplorer.org/challenges/1-launch-of-the-complexity-challenges).

Before running the model, you need at least the following softwares/Python packages:

1. Python 3.5+
2. [Mesa](https://github.com/projectmesa/mesa)
3. [Scipy](https://www.scipy.org/)
4. [Numpy](http://www.numpy.org/)

To run the model: 
```python
python r_model_server.py N 100 500 R 
```

where:

+ `N` is the number of new agents to be generated at each time step;

+ `R` is the __R neighborhood availability__ of each agent;

+ `100` is the board size (I make this variable so that I can do a better demo on it);

+ `500` is the size to show the board in the browser. 

After you run this code, a browser window will pop up and you can test the code out!