import sys
from r_model import CheckerBoardModel
##########################################
from mesa.visualization.modules import CanvasGrid, ChartModule
from mesa.visualization.ModularVisualization import ModularServer

def agent_portrayal(agent):
    portrayal = {"Shape": "circle",
                 "Color": "black",
                 "Filled": "true",
                 "text": str(agent.unique_id),
                 "text_color": "white",
                 "Layer": 0,
                 "r": 0.7}
    return portrayal

if __name__ == '__main__':
    chart = ChartModule([{"Label": "agentsOnBoard",
                          "Color": "Black"}],
                        data_collector_name='datacollector')

    numAgents, boardSize, pixel, R = sys.argv[1:5]

    grid = CanvasGrid(
                      agent_portrayal,
                      int(boardSize), int(boardSize), # 10x10 grid
                      int(pixel), int(pixel) # 500x500 pixels
                     )
    server = ModularServer(CheckerBoardModel,
                       [grid, chart],
                       "Checker Board",
                       # input arg dictionary
                       {"numAgents": int(numAgents),
                        "boardSize": int(boardSize),
                        "R": int(R)})

    server.port = 8899
    server.launch()
