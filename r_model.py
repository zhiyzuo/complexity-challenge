import warnings
from itertools import product
#############################
import numpy as np
import pandas as pd
from scipy.spatial.distance import cityblock
############################################
from mesa import Agent, Model
from mesa.space import MultiGrid
from mesa.time import BaseScheduler
from mesa.datacollection import DataCollector
#############################################
from utils import countAgentsOnBoard, repulsiveProbability


class MyGrid(MultiGrid):
    def __init__(self, height, width, torus=False):
        super().__init__(height=height, width=width, torus=torus)
    def my_get_neighborhood(self, pos, radius):
        neighborhood = super().iter_neighborhood(pos=pos, radius=radius, 
                                                 moore=True, include_center=False)
        return [cell for cell in neighborhood if cityblock(pos, cell) <= radius]
    

class MyActivation(BaseScheduler):
    ''' A customized activation scheduler to move agents simultaneously and break ties randomly. '''

    def break_ties(self):
        ''' Break ties randomly'''
        for agent in self.agents[:]:
            if agent.exitday is None:
                agent.find_newPos()

        agent_pos_df = pd.DataFrame([(agent, agent.newPos) for agent in self.agents[:] if agent.exitday is None],
                                    columns=['agent', 'newPos'])
        for newPos, df in agent_pos_df.groupby('newPos'):
            ''' randomly select one agent to move the that `newPos`'''
            selected_agent = df.agent[np.random.choice(df.agent.index)]
            for agent in df.agent:
                if agent is selected_agent:
                    continue
                ## agents not selected will stay put
                ## by setting `newPos` to `None`.
                agent.newPos = None

    def check_availability(self):
        '''Check if agents' `newPos` are available'''
        for agent in self.agents[:]:
            if agent.newPos is None:
                continue
            ## for those who are going to move, check if their future `dest` is available
            newPos_content_list = agent.model.grid.get_cell_list_contents(agent.newPos)
            for newPos_content in newPos_content_list:
                if newPos_content.newPos is None:
                    ## if it cannot move, reset `newPos`
                    agent.newPos = None
                    break

    def check_avail_wrapper(self):
        '''Due to the problem of synchronization, repeat doing this until converges'''
        converge = False
        last_newPos_array = np.asarray([agent.newPos for agent in self.agents[:]])
        while not converge:
            self.check_availability()
            this_newPos_array = np.asarray([agent.newPos for agent in self.agents[:]])
            if np.array_equal(this_newPos_array, last_newPos_array):
                converge = True
            last_newPos_array = np.asarray([agent.newPos for agent in self.agents[:]])

    def step(self):
        self.break_ties()
        self.check_avail_wrapper()
        for agent in self.agents[:]:
            if agent.exitday is None:
                agent.step()

class CheckerAgent(Agent):
    '''
        A checker on the board showing on the left-most column
        and trying to go to the right-most column.

        Attributes
        ----------
        aid : the index among all agents born at timestamp `tid`
        model: the board that an agent will be in
        dest: destination
        birthday: the timestamp when an agent shows up in the left-most column
        exitday: the timestamp when an agent arrives at its `dest`
        R : `r-neighborhood availability`
    '''
    def __init__(self, aid, model, dest, birthday):
        super().__init__(aid, model)
        self.dest = dest
        self.newPos = None
        self.birthday = birthday
        self.exitday = None
        self.R = self.model.R

    def find_newPos(self):
        #print('--Find-newPos--')
        current_dist = cityblock(self.pos, self.dest)
        possible_move_positions = list()
        radius = np.max((1, self.R))
        for neighbor in self.model.grid.my_get_neighborhood(pos=self.pos,
                                                            radius=radius):
            ## distance between the neighbor and its destination
            _dist = cityblock(neighbor, self.dest)
            if _dist < current_dist:
                #possible_move_positions.append((neighbor,
                #                                self.model.grid.is_cell_empty(neighbor)))
                possible_move_positions.append(neighbor)

        ## `possible_move_positions` contain further-away cells (dist > 1)
        ## `r_neighborhood` would be those more than 1-hop away from `self.pos`
        r_neighborhood = [cell for cell in possible_move_positions if cityblock(cell, self.pos) > 1]
        ## `possible_move_positions` updated as those distances == 1 with `self.pos`
        possible_move_positions = [cell for cell in possible_move_positions if cell not in r_neighborhood]
        ## if more than two possible directions, raise Exception
        if len(possible_move_positions) > 2:
            #print(possible_move_positions, r_neighborhood)
            raise Exception('More than two possible `newPos`\'s')
        ## if no possible directions, raise Exception
        elif len(possible_move_positions) < 1:
            raise Exception('Should have been removed!')

        ## only 1 possible choice, choose it
        if len(possible_move_positions) < 2:
            self.newPos  = possible_move_positions[0]
            return

        ## if there are two
        newPosProbability = (np.ones(shape=(len(possible_move_positions),), dtype=float)/len(possible_move_positions)).tolist()
        ## with no knowledge of the neighborhood, randomly choose one with equal probability
        ## else, check neighborhood availability
        if self.R > 0:
            for index in np.arange(len(possible_move_positions)):
                newPos = possible_move_positions[:][index]

                ## `r-1` neighborhood
                newPos_neighbor = [cell for cell in r_neighborhood if cityblock(cell, newPos) <= self.R-1]
                newPos_neighbor += [newPos]

                ## not empty, decrease probability
                newPosProbability[index] -= sum([repulsiveProbability(cityblock(self.pos, cell))
                                                 if not self.model.grid.is_cell_empty(cell) else 0 \
                                                 for cell in newPos_neighbor ])
        ## normalize probability
        newPosProbability = np.asarray(newPosProbability)/float(np.sum(newPosProbability))
        #print(self.unique_id, self.pos, possible_move_positions, newPosProbability)
        self.newPos = possible_move_positions[np.random.choice(len(possible_move_positions),
                                                               p=newPosProbability)]

        #print('--Done-find-newPos--')

    def step(self):
        if self.newPos is not None:
            self.model.grid.move_agent(self, self.newPos)
        if self.pos == self.dest:
            ## upon arrival, remove itself from the grid
            self.model.grid.remove_agent(self)
            #self.model.schedule.remove(self)
            self.exitday = self.model.timestamp
            #print('Agent %d exits at time %d'%(self.unique_id, self.exitday))


class CheckerBoardModel(Model):
    '''
        A checker board bearing the agents.
        Each time step, a constant of agents will be added to the left-most column if avaialble.

        Attributes
        ----------
        aid : the index among all agents born at timestamp `tid`
        numAgents : the number of agents generated at each timestamp
        boardSize : the width and height of the grid
        R : `R-neighborhood availability`
        schedule : the scheduler to use
        timestamp : the current timestamp (starts at 0)
    '''
    def __init__(self, numAgents, boardSize, R=0):
        super().__init__(np.random.randint(0, 100))
        ## number of agents
        self.running = True
        if numAgents > boardSize:
            warnings.warn("# of agents is larger than the board size! Use board size %d as the number of agents"%boardSize,
                           RuntimeWarning)
            numAgents = boardSize
        self.numAgents = np.min((numAgents, boardSize))
        self.R = R
        ## Not toroidial: cannot go across the border.
        self.grid = MyGrid(boardSize, boardSize, False)
        self.schedule = MyActivation(self)
        self.timestamp = 0
        #self.cellCoords = list(product(np.arange(self.grid.width), 
        #                                np.arange(self.grid.width)))
        self.datacollector = DataCollector(
                                           model_reporters={"agentsOnBoard": countAgentsOnBoard},
                                           agent_reporters={'birthday': lambda a: a.birthday,
                                                            'exitday': lambda a: a.exitday}
                                          )
        #print('Current timestamp %d'%self.timestamp)
        ## Create agents
        for i in np.arange(self.numAgents):
            a = CheckerAgent(aid=i+1,
                             model=self,
                             birthday = self.timestamp,
                             dest=(boardSize-1,
                                   np.random.choice(self.grid.width)))
            ## destinations are sampled on the right-most column
            self.schedule.add(a)
            ## place they on the left-most column
            while True:
                xi = np.random.choice(self.grid.width)
                if self.grid.is_cell_empty((0, xi)):
                    self.grid.place_agent(a, (0, xi))
                    a.newPos = (0, xi)
                    break
                else:
                    continue
            
    def generate_new_agents(self):
        '''Generate new agents to the board if avaialble, AFTER step function'''
        ## check left-most columns availability
        available_cells = [xi for xi in np.arange(self.grid.width) \
                           if self.grid.is_cell_empty((0, xi))]
        numNewAgents = np.min((len(available_cells), self.numAgents))
        ## if no available slots, do not generate new agents
        if numNewAgents < 1:
            return
        ## re-order the availabilities randomly
        random_indices = np.random.choice(range(len(available_cells)), 
                                          replace=False, 
                                          size=numNewAgents)
        available_cells = [available_cells[i] for i in random_indices]
                                                                        
        ## index of the emerging agents: current max + 1
        starting_idx = max([a.unique_id for a in self.schedule.agents[:]]) + 1
        ## randomly assign positions (for k agents, k is the min of board size and availability)
        for i in np.arange(len(available_cells)):
            a = CheckerAgent(aid=starting_idx+i,
                             model=self,
                             birthday=self.timestamp,
                             dest=(self.grid.width-1,
                                   np.random.choice(self.grid.width)))
            self.schedule.add(a)
            self.grid.place_agent(a, (0, available_cells[i]))
            
    def check_cell_content(self):
        '''Check if one cell contains more than one agent at one timestamp after `step` and `generate` is done.'''
        cell_agent_count = dict()
        for cell in self.grid.coord_iter():
            cell_agent_count[cell[1:]] = len(cell[0])
        violated_cells = [cell for cell in cell_agent_count 
                          if cell_agent_count[cell] > 1]
        if len(violated_cells) > 0:
            #print(violated_cells)
            raise Exception('More than two agents on one cell')
                
        
    def step(self):
        '''Advance the model by one step.
        
            One step includes:
            1) Move existing nodes using scheduler
            2) Generate new agents
            3) increment timestamp
        '''
        self.datacollector.collect(self)
        self.schedule.step()
        self.generate_new_agents()
        self.timestamp += 1
        self.check_cell_content()
        #print('Current timestamp %d'%self.timestamp)
        #print('-----------------------------------')
