import warnings
import numpy as np
import pandas as pd
from scipy.spatial.distance import cityblock
############################################
from mesa import Agent, Model
from mesa.space import MultiGrid
from mesa.time import BaseScheduler

class MyActivation(BaseScheduler):
    ''' A customized activation scheduler to move agents simultaneously and break ties randomly. '''

    def step(self):
        ''' Break ties randomly'''
        for agent in self.agents[:]:
            if not agent.arrival:
                agent.find_new_pos()
            if agent.arrival and not agent.removed:
                agent.model.grid.remove_agent(agent)
                agent.removed = True

        agent_pos_df = pd.DataFrame([(agent, agent.new_pos) for agent in self.agents[:] \
                                     if not agent.arrival],
                                    columns=['agent', 'new_pos'])
        for new_pos, df in agent_pos_df.groupby('new_pos'):
            ''' randomly select one agent to move the that `new_pos`'''
            selected_agent = df.agent[np.random.choice(df.agent.index)]
            for agent in df.agent:
                if agent is selected_agent:
                    continue
                else:
                    ## agents not selected will stay put
                    ## by moving to the same position.
                    agent.new_pos = agent.pos

        for agent in self.agents[:]:
            if not agent.arrival:
                agent.step()

class CheckerAgent(Agent):
    '''
        A checker on the board showing on the left-most column
        and trying to go to the right-most column.

        Attributes
        ----------
        aid : the index among all agents born at timestamp `tid`
        dest: destination
        model: the board that an agent will be in
    '''
    def __init__(self, aid, model, dest):
        super().__init__(aid, model)
        self.dest = dest
        self.new_pos = None
        self.arrival = False
        self.removed = False

    def find_new_pos(self):
        if cityblock(self.pos, self.dest) == 0:
            self.arrival = True
        else:
            current_dist = cityblock(self.pos, self.dest)
            possible_move_positions = list()
            for neighbor in self.model.grid.iter_neighborhood(self.pos, False):
                ## distance between the neighbor and its destination
                _dist = cityblock(neighbor, self.dest)
                if _dist < current_dist:
                    #possible_move_positions.append((neighbor, 
                    #                                self.model.grid.is_cell_empty(neighbor)))
                    possible_move_positions.append(neighbor)
            self.new_pos = possible_move_positions[np.random.choice(len(possible_move_positions))]

    def step(self):
        # The agent's step will go here.
        self.model.grid.move_agent(self, self.new_pos)

class CheckerBoardModel(Model):
    '''
        A checker board bearing the agents.

        Attributes
        ----------
        aid : the index among all agents born at timestamp `tid`
    '''
    def __init__(self, numAgents, boardSize, seed=None):
        super().__init__(seed)
        ## number of agents
        if numAgents > boardSize:
             warnings.warn("# of agents is larger than the board size! Use board size %d as the number of agents"%boardSize,
                           RuntimeWarning)
        self.numAgents = np.min((numAgents, boardSize))
        ## Not toroidial: cannot go across the border.
        self.grid = MultiGrid(boardSize, boardSize, False)
        self.schedule = MyActivation(self)
        ## Create agents
        for i in np.arange(self.numAgents):
            a = CheckerAgent(aid=i+1,
                             model=self,
                             dest=(boardSize-1,
                                   np.random.choice(self.grid.width)))
            self.schedule.add(a)
            ## place they on the left-most column
            while True:
                xi = np.random.choice(self.grid.width)
                if self.grid.is_cell_empty((0, xi)):
                    self.grid.place_agent(a, (0, xi))
                    a.new_pos = (0, xi)
                    break
                else:
                    continue
            ## destinations are sampled with replacement
            ## on the right-most column
    def step(self):
        '''Advance the model by one step.'''
        if sum([1 for agent in self.schedule.agents if agent.arrival]) == self.numAgents:
            print('No agents any more')
            return
        self.schedule.step()

