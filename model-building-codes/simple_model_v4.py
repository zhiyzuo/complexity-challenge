import warnings
from itertools import product
#############################
import numpy as np
import pandas as pd
from scipy.spatial.distance import cityblock
############################################
from mesa import Agent, Model
from mesa.space import MultiGrid
from mesa.time import BaseScheduler
from mesa.datacollection import DataCollector
#############################################
from utils import countAgentsOnBoard


class MyActivation(BaseScheduler):
    ''' A customized activation scheduler to move agents simultaneously and break ties randomly. '''

    def break_ties(self):
        ''' Break ties randomly'''
        for agent in self.agents[:]:
            if agent.exitday is None:
                agent.find_newPos()
            
        agent_pos_df = pd.DataFrame([(agent, agent.newPos) for agent in self.agents[:]],
                                    columns=['agent', 'newPos'])
        for newPos, df in agent_pos_df.groupby('newPos'):
            ''' randomly select one agent to move the that `newPos`'''
            selected_agent = df.agent[np.random.choice(df.agent.index)]
            for agent in df.agent:
                if agent is selected_agent:
                    continue
                ## agents not selected will stay put
                ## by setting `newPos` to `None`.
                agent.newPos = None
            print(newPos, [item.unique_id for item in df.agent], selected_agent.unique_id, 
                  [(item.unique_id, item.pos, item.newPos) for item in df.agent if item != selected_agent])
    
    def check_availability(self):
        '''Check if agents' `newPos` are available'''
        for agent in self.agents[:]:
            if agent.newPos is None:
                continue
            ## for those who are going to move, check if their future `dest` is available
            newPos_content_list = agent.model.grid.get_cell_list_contents(agent.newPos)
            for newPos_content in newPos_content_list:
                if newPos_content.newPos is None:
                    ## if it cannot move, reset `newPos`
                    agent.newPos = None
                    break
            print(agent.unique_id, agent.pos, agent.newPos, 
                  [(a.unique_id, a.pos, a.newPos) for a in newPos_content_list])

    def check_avail_wrapper(self):
        '''Due to the problem of synchronization, repeat doing this until converges'''
        converge = False
        last_newPos_array = np.asarray([agent.newPos for agent in self.agents[:]])
        while not converge:
            self.check_availability()
            this_newPos_array = np.asarray([agent.newPos for agent in self.agents[:]])
            if np.array_equal(this_newPos_array, last_newPos_array):
                converge = True
            last_newPos_array = np.asarray([agent.newPos for agent in self.agents[:]])
            
    def step(self):
        self.break_ties()
        self.check_avail_wrapper()
        for agent in self.agents[:]:
            if agent.exitday is None:
                agent.step()


class CheckerAgent(Agent):
    '''
        A checker on the board showing on the left-most column
        and trying to go to the right-most column.

        Attributes
        ----------
        aid : the index among all agents born at timestamp `tid`
        model: the board that an agent will be in
        dest: destination
        birthday: the timestamp when an agent shows up in the left-most column
        exitday: the timestamp when an agent arrives at its `dest`
    '''
    def __init__(self, aid, model, dest, birthday):
        super().__init__(aid, model)
        self.dest = dest
        self.newPos = None
        self.birthday = birthday
        self.exitday = None

    def find_newPos(self):
        current_dist = cityblock(self.pos, self.dest)
        possible_move_positions = list()
        for neighbor in self.model.grid.iter_neighborhood(self.pos, False):
            ## distance between the neighbor and its destination
            _dist = cityblock(neighbor, self.dest)
            if _dist < current_dist:
                #possible_move_positions.append((neighbor, 
                #                                self.model.grid.is_cell_empty(neighbor)))
                possible_move_positions.append(neighbor)
        ## randomly pick one direction to proceed to `dest` without prior knowledge.
        self.newPos = possible_move_positions[np.random.choice(len(possible_move_positions))]

    def step(self):
        if self.newPos is not None:
            self.model.grid.move_agent(self, self.newPos)
        if self.pos == self.dest:
            ## upon arrival, remove itself from the grid
            self.model.grid.remove_agent(self)
            #self.model.schedule.remove(self)
            self.exitday = self.model.timestamp
            print('Agent %d exits at time %d'%(self.unique_id, self.exitday))


class CheckerBoardModel(Model):
    '''
        A checker board bearing the agents.
        Each time step, a constant of agents will be added to the left-most column if avaialble.

        Attributes
        ----------
        aid : the index among all agents born at timestamp `tid`
        numAgents : the number of agents generated at each timestamp
        boardSize : the width and height of the grid
        schedule : the scheduler to use
        timestamp : the current timestamp (starts at 0)
    '''
    def __init__(self, numAgents, boardSize, seed=None):
        super().__init__(seed)
        ## number of agents
        if numAgents > boardSize:
            warnings.warn("# of agents is larger than the board size! Use board size %d as the number of agents"%boardSize,
                           RuntimeWarning)
            numAgents = boardSize
        self.numAgents = np.min((numAgents, boardSize))
        ## Not toroidial: cannot go across the border.
        self.grid = MultiGrid(boardSize, boardSize, False)
        self.schedule = MyActivation(self)
        self.timestamp = 0
        #self.cellCoords = list(product(np.arange(self.grid.width), 
        #                                np.arange(self.grid.width)))
        self.datacollector = DataCollector(
                                           model_reporters={"agentsOnBoard": countAgentsOnBoard},
                                           agent_reporters={'birthday': lambda a: a.birthday,
                                                            'exitday': lambda a: a.exitday}
                                          )
        #print('Current timestamp %d'%self.timestamp)
        ## Create agents
        for i in np.arange(self.numAgents):
            a = CheckerAgent(aid=i+1,
                             model=self,
                             birthday = self.timestamp,
                             dest=(boardSize-1,
                                   np.random.choice(self.grid.width)))
            ## destinations are sampled on the right-most column
            self.schedule.add(a)
            ## place they on the left-most column
            while True:
                xi = np.random.choice(self.grid.width)
                if self.grid.is_cell_empty((0, xi)):
                    self.grid.place_agent(a, (0, xi))
                    a.newPos = (0, xi)
                    break
                else:
                    continue
            
    def generate_new_agents(self):
        '''Generate new agents to the board if avaialble, AFTER step function'''
        ## check left-most columns availability
        available_cells = [xi for xi in np.arange(self.grid.width) \
                           if self.grid.is_cell_empty((0, xi))]
        numNewAgents = np.min((len(available_cells), self.numAgents))
        ## if no available slots, do not generate new agents
        if numNewAgents < 1:
            return
        ## re-order the availabilities randomly
        random_indices = np.random.choice(range(len(available_cells)), 
                                          replace=False, 
                                          size=numNewAgents)
        available_cells = [available_cells[i] for i in random_indices]
                                                                        
        ## index of the emerging agents: current max + 1
        starting_idx = max([a.unique_id for a in self.schedule.agents[:]]) + 1
        ## randomly assign positions (for k agents, k is the min of board size and availability)
        for i in np.arange(len(available_cells)):
            a = CheckerAgent(aid=starting_idx+i,
                             model=self,
                             birthday=self.timestamp,
                             dest=(self.grid.width-1,
                                   np.random.choice(self.grid.width)))
            self.schedule.add(a)
            self.grid.place_agent(a, (0, available_cells[i]))
            
    def check_cell_content(self):
        '''Check if one cell contains more than one agent at one timestamp after `step` and `generate` is done.'''
        cell_agent_count = dict()
        for cell in self.grid.coord_iter():
            cell_agent_count[cell[1:]] = len(cell[0])
        violated_cells = [cell for cell in cell_agent_count 
                          if cell_agent_count[cell] > 1]
        if len(violated_cells) > 0:
            print(violated_cells)
            raise Exception('More than two agents on one cell')
                
        
    def step(self):
        '''Advance the model by one step.
        
            One step includes:
            1) Move existing nodes using scheduler
            2) Generate new agents
            3) increment timestamp
        '''
        self.datacollector.collect(self)
        self.schedule.step()
        self.generate_new_agents()
        self.timestamp += 1
        self.check_cell_content()
        print('Current timestamp %d'%self.timestamp)
        print('-----------------------------------')
