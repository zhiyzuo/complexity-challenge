import sys
from simple_model_v2 import CheckerBoardModel
##########################################
from mesa.visualization.modules import CanvasGrid
from mesa.visualization.ModularVisualization import ModularServer

def agent_portrayal(agent):
    portrayal = {"Shape": "circle",
                 "Color": "red",
                 "Filled": "true",
                 "text": str(agent.unique_id),
                 "text_color": "black",
                 "Layer": 0,
                 "r": 0.3}
    return portrayal

if __name__ == '__main__':
    numAgents, boardSize = sys.argv[1:3]

    grid = CanvasGrid(
                      agent_portrayal,
                      int(boardSize), int(boardSize), # 10x10 grid
                      500, 500 # 500x500 pixels
                     )
    server = ModularServer(CheckerBoardModel,
                       [grid],
                       "Checker Board",
                       # input arg dictionary
                       {"numAgents": int(numAgents),
                        "boardSize": int(boardSize)})

    server.port = 8899
    server.launch()
