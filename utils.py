import numpy as np

def repulsiveProbability(d):
    return 1/(3.01*np.exp(d))

def countAgentsOnBoard(model):
    '''Count the # of agents on a board'''
    return sum([not model.grid.is_cell_empty(cell[1:]) \
                for cell in model.grid.coord_iter()])
